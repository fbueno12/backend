import express from "express";
import routes from './routes';

const app = express();

app.use(routes);

app.listen(8080, () => {
    console.log("\u{2708} \u{2708} \u{2708} \u{2708} Server is running at port 8080");
})