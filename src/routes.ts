import { Router, Request, Response } from 'express';

const routes = Router();

routes.get("/", (request: Request, response: Response): void => {
    response.send("Hello World");
})

export default routes;